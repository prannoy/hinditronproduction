package com.hinditron.pipeline.spark.utility;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.*;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by prannoysircar on 6/3/16.
 */
public class OutlierFileDetection {

        Config config = ConfigFactory.load("dev.conf");

    public void newFileLookup()throws IOException,
            InterruptedException {

        String fileSource = config.getString("original_threshold_file_path");


        Path faxFolder = Paths.get(fileSource);
        WatchService watchService = FileSystems.getDefault().newWatchService();
        faxFolder.register(watchService, StandardWatchEventKinds.ENTRY_CREATE,StandardWatchEventKinds.ENTRY_MODIFY);

        boolean valid = true;
        do {
            WatchKey watchKey = watchService.take();

            for (WatchEvent event : watchKey.pollEvents()) {
                WatchEvent.Kind kind = event.kind();
                if (StandardWatchEventKinds.ENTRY_MODIFY.equals(event.kind()) || StandardWatchEventKinds.ENTRY_CREATE.equals(event.kind())) {
                    String fileName = event.context().toString();
                    try {
                        String filePathInLocal = fileSource + "/" + fileName;
                        if(!fileName.contains(".swp") && !fileName.contains("4913") && fileName.contains("threshold")){
                          boolean checkFileFormatStatus = new FormatValidator().checkThresholdFileFormat(fileSource + "/" , fileName);
                            if(checkFileFormatStatus) {

                                String lastModifiedTimeStamp = getLastModifiedTimeStamp(filePathInLocal, fileName);
                                File newFile = new File(filePathInLocal);
                                File copiedFile = new File(config.getString("copied_threshold_file_path") + "/" + "threshold.txt");
                                FileUtils.copyFile(newFile, copiedFile);
                                System.out.println("File copied to copied location");
                                File backupFile = new File(config.getString("backup_file_path") + "/" + "threshold.txt" + "_" + lastModifiedTimeStamp);
                                FileUtils.copyFile(newFile, backupFile);
                                System.out.println("File copied to backup location");
                                new WatcherService().mailer("New threshold file is uploaded. Filename : " + fileName ,"NEW THRESHOLD FILE UPLOADED",config.getString("email_recipient_list"));

                            }

                            else {
                                //Send alert mail for format issues
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            valid = watchKey.reset();

        } while (valid);

    }

    public  String getLastModifiedTimeStamp (String filePathInLocal ,String fileName){
        File newFile = new File(filePathInLocal);
        Long lastModifiedTimeStamp = newFile.lastModified();
        Date date = new Date(lastModifiedTimeStamp);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String timeStamp = sdf.format(date);
        timeStamp = timeStamp.replaceAll("[^A-Za-z0-9]", "_");
        System.out.println(timeStamp);
        return timeStamp;
    }

    public String getDateInString(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String timeStamp = sdf.format(date);
        timeStamp = timeStamp.replaceAll("[^A-Za-z0-9]", "_");
        return timeStamp;
    }

    }
