package com.hinditron.pipeline.spark.utility;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
/**
 * Created by prannoysircar on 7/1/16.
 */
public class WatcherService {

    Config config = ConfigFactory.parseFile(new File("/home/administrator/hinditron/hinditrondev/src/main/resources/dev.conf"));

    public void watcherServiceMainCall()  throws IOException,
            InterruptedException{

        String EMAIL_RECIPIENT_LIST = config.getString("email_recipient_list");



        /*
        * Directory location to be watched
        */

        String SOURCE_ONE     =  config.getString("apache_error_log_path");
//        String SOURCE_TWO     =  config.getString("path_xml_pipeline_log");
//        String SOURCE_THREE   =  config.getString("path_csv_pipeline_log");

        Path WATCHER_PATH_ONE = Paths.get(SOURCE_ONE);
//        Path WATCHER_PATH_TWO = Paths.get(SOURCE_TWO);
//        Path WATCHER_PATH_THREE = Paths.get(SOURCE_TWO);


        WatchService watchService = FileSystems.getDefault().newWatchService();

        /*
        * Registration of paths to be watched
        */

        WATCHER_PATH_ONE.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
//        WATCHER_PATH_TWO.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
//        WATCHER_PATH_THREE.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);


        boolean valid = true;
        do {
            WatchKey watchKey = watchService.take();

            for (WatchEvent event : watchKey.pollEvents()) {
                WatchEvent.Kind kind = event.kind();
                if (StandardWatchEventKinds.ENTRY_MODIFY.equals(event.kind())) {
                    String fileName = event.context().toString();

                    if(!fileName.contains(".swp") && !fileName.contains("4913")){

                        if(fileName.equals("error.log")){

                            checkInvalidLoginAttempt(SOURCE_ONE, fileName);

                        }
//                         if(fileName.equals("")){
//
//                            checkRabbitMQErrorLog(SOURCE_TWO, fileName);
//
//                        }
//                         if(fileName.equals("error.log")){
//
//                            checkCSVErrorLog(SOURCE_THREE, fileName);
//
//                        }
                        System.out.println("New line in file :" + fileName);

                    }
                }
            }
            valid = watchKey.reset();

        } while (valid);

    }


    public void checkInvalidLoginAttempt(String SOURCE, String FILENAME){


        String lastLines = tail(new File(SOURCE + "/" + FILENAME), 2);

        if (lastLines.contains("authorization failure")){

            String emailBody = "Invalid login attempt. Below are the logs\n\n" + lastLines;
            String emailSubject = "Invalid Login attempt";


            mailer(emailBody, emailSubject,config.getString("email_recipient_list"));

        }

    }

    public void checkRabbitMQErrorLog(String SOURCE, String FILENAME){

        String lastLines = tail(new File(SOURCE + "/" + FILENAME), 2);

        String emailBody = "";
        String emailSubject = "";


        mailer(emailBody, emailSubject,"");


    }

    public void checkCSVErrorLog(String SOURCE, String FILENAME){

        String lastLines = tail(new File(SOURCE + "/" + FILENAME), 2);

        String emailBody = "";
        String emailSubject = "";


        mailer(emailBody, emailSubject,"");

    }

    public void mailer(String EMAIL_BODY, String EMAIL_SUBJECT, String EMAIL_RECIPIENT_LIST){

        try{
            System.out.println("INSIDE MAILER");
            String command = "echo \"" + EMAIL_BODY + "\" | mail -s \"" + EMAIL_SUBJECT + "\" " + EMAIL_RECIPIENT_LIST;
            ProcessBuilder processBuilder = new ProcessBuilder("bash", "-c", command);
            processBuilder.start();
        }

        catch (Exception e){

            e.printStackTrace();

        }


    }




    public static String tail(File file, int lines) {
        java.io.RandomAccessFile fileHandler = null;
        try {
            fileHandler =
                    new java.io.RandomAccessFile( file, "r" );
            long fileLength = fileHandler.length() - 1;
            StringBuilder sb = new StringBuilder();
            int line = 0;

            for(long filePointer = fileLength; filePointer != -1; filePointer--){
                fileHandler.seek( filePointer );
                int readByte = fileHandler.readByte();

                if( readByte == 0xA ) {
                    if (filePointer < fileLength) {
                        line = line + 1;
                    }
                } else if( readByte == 0xD ) {
                    if (filePointer < fileLength-1) {
                        line = line + 1;
                    }
                }
                if (line >= lines) {
                    break;
                }
                sb.append( ( char ) readByte );
            }

            String lastLine = sb.reverse().toString();
            return lastLine;
        } catch( java.io.FileNotFoundException e ) {
            e.printStackTrace();
            return null;
        } catch( java.io.IOException e ) {
            e.printStackTrace();
            return null;
        }
        finally {
            if (fileHandler != null )
                try {
                    fileHandler.close();
                } catch (IOException e) {
                }
        }
    }
}

