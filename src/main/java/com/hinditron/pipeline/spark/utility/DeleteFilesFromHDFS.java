package com.hinditron.pipeline.spark.utility;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

/**
 * Created by prannoysircar on 8/3/16.
 */
public class DeleteFilesFromHDFS {

    Config config = ConfigFactory.parseFile(new File("/home/administrator/hinditron/hinditrondev/src/main/resources/dev.conf"));

    public void watcherServiceMainCall()  throws IOException,
            InterruptedException{

        String EMAIL_RECIPIENT_LIST = config.getString("email_recipient_list");

        /*
        * Directory location to be watched
        */

        String SOURCE_ONE     =  config.getString("sftp_gate_way_backup_source_path");

        Path WATCHER_PATH_ONE = Paths.get(SOURCE_ONE);

        WatchService watchService = FileSystems.getDefault().newWatchService();

        /*
        * Registration of paths to be watched
        */

        WATCHER_PATH_ONE.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);


        boolean valid = true;
        do {
            WatchKey watchKey = watchService.take();

            for (WatchEvent event : watchKey.pollEvents()) {
                WatchEvent.Kind kind = event.kind();
                if (StandardWatchEventKinds.ENTRY_CREATE.equals(event.kind())) {
                    String fileName = event.context().toString();

                    if(!fileName.contains(".swp") && !fileName.contains("4913")){

                        new SftpToHdfsTransfer().deleteFileFromHDFS(config.getString("hdfs_csv_backup"), fileName);


                    }
                }
            }
            valid = watchKey.reset();

        } while (valid);

    }

}
