package com.hinditron.pipeline.spark.utility;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.FileWriter;
import java.util.Date;
import java.sql.Timestamp;


/**
 * Created by prannoysircar on 6/9/16.
 */
public class LogWriter {

    public void logger(String PATH, boolean APPEND_TO_FILE_FLAG, String LOG_LINE) {

        try {
            FileWriter writer = new FileWriter(PATH, APPEND_TO_FILE_FLAG);
            writer.write(LOG_LINE);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void logToHDFS(String HOST, String PATH, String CONTENT_TO_WRITE){
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS", HOST);
        conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
        System.out.println("Connecting to -- " + conf.get("fs.defaultFS"));
        Date date= new Date();
        String outName = (new Timestamp(date.getTime())).toString();
        outName = outName.replaceAll("[^A-Za-z0-9]", "_");

        Path filePath = new Path(PATH+ outName+".txt");


        try{
            FileSystem fs = FileSystem.get(conf);
            FSDataOutputStream fsStream = fs.create(filePath);
            fsStream.write(CONTENT_TO_WRITE.getBytes());
            fsStream.close();
        }

        catch (Exception e) {

            e.printStackTrace();
        }
    }
}
