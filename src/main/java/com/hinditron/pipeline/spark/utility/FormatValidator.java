package com.hinditron.pipeline.spark.utility;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.avro.test.Mail;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Date;

/**
 * Created by prannoysircar on 7/22/16.
 */
public class FormatValidator {

    Config config = ConfigFactory.load("dev.conf");


    public boolean checkCSVformat(String filePathInLocal, String fileName) {
        BufferedReader reader = null;
        boolean isValidCSVFormat = true;

        filePathInLocal = filePathInLocal + fileName;

        try {
            if (fileName.contains("XRF") || fileName.contains("OES")) {
                long fileSize = new File(filePathInLocal).length();
                if (fileSize == 0){
                    Thread.sleep(2000);
                }

                System.out.println("Size of file " + fileSize);
                if (new File(filePathInLocal).length() > 0) {

                    reader = new BufferedReader(new FileReader(filePathInLocal));
                    String temp = "";
                    temp = reader.readLine();

                    while (temp != null) {
                        String[] splitValue = temp.split(",");

                        boolean isIndexesPresent = true;

                        if (fileName.contains("XRF")){
                            validateXRFIndexes(temp);
                        }
                        if (fileName.contains("OES")){
                            validateOESIndexes(temp);
                        }


                        if (splitValue.length >= 24) {
                            boolean checkNumberOfElementsValueFormat = true;
                            String numberOfElementValue = splitValue[24].trim();
                            try {
                                Integer.parseInt(numberOfElementValue);
                            } catch (NumberFormatException e) {
                                checkNumberOfElementsValueFormat = false;
                            } catch (Exception e) {
                                checkNumberOfElementsValueFormat = false;
                            }

                            if (checkNumberOfElementsValueFormat) {
                                int numberOfElementsInCSV = ((splitValue.length - 1) - 24) / 3;

                                if (numberOfElementsInCSV == Integer.parseInt(numberOfElementValue)) {

                                    String materialCode = splitValue[23];
                                    String materialGroup = splitValue[22];
                                    if (!materialCode.isEmpty() && !materialGroup.isEmpty()) {

                                        int validatorCount = 0;
                                        String invalidType = "";

                                        for (int i = 25; i < splitValue.length; i = i + 3) {
                                            String elementName = splitValue[i].trim();
                                            String elementValue = splitValue[i + 2].trim();

                                            if (elementName.isEmpty() || NumberUtils.isNumber(elementName) || elementValue.isEmpty() || !NumberUtils.isNumber(elementValue)) {
                                                validatorCount++;
                                                invalidType = invalidType + "Element Name : " + elementName + "," + " Element Value : " + elementValue + "\n";
                                            }
                                        }

                                        if (validatorCount != 0) {

                                            new LogWriter().logger(config.getString("csv_invalid_format_log"),true,"Logged on " + new Date()+ "\n" + "FILE_NAME : " + fileName + " " +"Invalid formats found for following elements\n" + invalidType);
                                            String message = "FILE_NAME : " + fileName + ". Element name or element value are not in required format. Please check the following list.\n" + invalidType +"\n";
                                            String subject = "INVALID INPUT CSV FORMAT";
                                            new WatcherService().mailer(message,subject,config.getString("email_recipient_list"));

                                            //alert for invalidtype of element name and value.
                                            isValidCSVFormat = false;
                                        }


                                    } else {
                                        new LogWriter().logger(config.getString("csv_invalid_format_log"),true,"Logged on "+ new Date()+ "FILE_NAME : " + fileName + " " + " Material code or material group is empty.\n");
                                        String message = "FILE_NAME : " + fileName + ". Material code or material group is empty.\n";
                                        String subject = "INVALID INPUT CSV FORMAT";
                                        new WatcherService().mailer(message,subject,config.getString("email_recipient_list"));
                                        //alert for material code and group is empty
                                        isValidCSVFormat = false;
                                    }


                                } else {
                                    new LogWriter().logger(config.getString("csv_invalid_format_log"),true,"Logged on " + new Date()+ "FILE_NAME : " + fileName + " " + " Number of elements value not equal to number of elements present in CSV.\n ");
                                    String message = "FILE_NAME : " + fileName + ". Number of elements value not equal to number of number of elements present in CSV.\n";
                                    String subject = "INVALID INPUT CSV FORMAT";
                                    new WatcherService().mailer(message,subject,config.getString("email_recipient_list"));
                                    isValidCSVFormat=false;
                                    //alert for number of elements not equal to number of elements present in csv
                                }


                            } else {
                                new LogWriter().logger(config.getString("csv_invalid_format_log"),true,"Logged on " + new Date()+ "FILE_NAME : " + fileName + " " + " Number of elements value not a integer.\n");
                                String message = "FILE_NAME : " + fileName + ". Number of elements value not a integer.\n";
                                String subject = "INVALID INPUT CSV FORMAT";
                                new WatcherService().mailer(message,subject,config.getString("email_recipient_list"));
                                //alert for number of elements not a integer
                                isValidCSVFormat = false;
                            }

                        } else {

                            new LogWriter().logger(config.getString("csv_invalid_format_log"),true,"Logged on " +  new Date()+ "FILE_NAME : " + fileName + " " + " Number of items present in CSV is less than 24.\n");
                            String message = "FILE_NAME : " + fileName + ". Number of items present in CSV is less than 24.\n";
                            String subject = "INVALID INPUT CSV FORMAT";
                            new WatcherService().mailer(message,subject,config.getString("email_recipient_list"));
                            //alert for length less than 24
                            isValidCSVFormat = false;
                        }


                        temp = reader.readLine();

                    }
                } else {
                    new LogWriter().logger(config.getString("csv_invalid_format_log"),true,"Logged on " +  new Date()+ "FILE_NAME : " + fileName + " " + " Input CSV file is empty.\n");
                    String message = "FILE_NAME : " + fileName + ". Input CSV file is empty.\n";
                    String subject = "INVALID INPUT CSV FORMAT";
                    isValidCSVFormat = false;
                    System.out.println("after coming to else  "+isValidCSVFormat);
                    new WatcherService().mailer(message,subject,config.getString("email_recipient_list"));
                    //alert for empty csv file
                }
            } else {
                new LogWriter().logger(config.getString("csv_invalid_format_log"),true,"Logged on " + new Date() + "FILE_NAME : " + fileName + " " + " Input CSV file is not having a known SOURCE type.\n");
                String message = "FILE_NAME : " + fileName + ". Input CSV file is not having a known SOURCE type.\n";
                String subject = "INVALID INPUT CSV FORMAT";
                new WatcherService().mailer(message,subject,config.getString("email_recipient_list"));
                isValidCSVFormat = false;
                //Invalid filename
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return isValidCSVFormat;

    }

    public boolean checkThresholdFileFormat(String filePathInLocal, String fileName) {
        BufferedReader reader = null;
        int validatorCount = 0;

        filePathInLocal = filePathInLocal + fileName;

        String MAIL_MESSAGE = "";

        try {
            reader = new BufferedReader(new FileReader(filePathInLocal));
            String temp = "";
            temp = reader.readLine();


            int lineCount = 0;
            while (temp != null) {
                boolean checkNumberOfElementsValueFormat = true;
                lineCount++;
                String[] splitValue = temp.split("\\|");

                if(splitValue.length>17){

                try {
                    Integer.parseInt(splitValue[17].trim());
                } catch (NumberFormatException e) {
                    checkNumberOfElementsValueFormat = false;
                } catch (Exception e) {
                    checkNumberOfElementsValueFormat = false;
                }


                if (checkNumberOfElementsValueFormat) {

                    int numberOfElementsValueFile = Integer.parseInt(splitValue[17].trim());
                    int numberOfElementsPresentInFile = (splitValue.length - 1 - 17) / 5;

                    boolean checkNumberOfElementsInFileIsEqualToNumberOfElementsValue = false;

                    if (numberOfElementsPresentInFile == numberOfElementsValueFile) {
                        checkNumberOfElementsInFileIsEqualToNumberOfElementsValue = true;
                    }


                    if (checkNumberOfElementsInFileIsEqualToNumberOfElementsValue) {

                        for (int i = 18; i < splitValue.length; i = i + 5) {
                            String elementName = splitValue[i].trim();
                            String firstElementValue = splitValue[i + 1].trim();
                            String secondElementValue = splitValue[i + 2].trim();
                            String thirdElementValue = splitValue[i + 3].trim();
                            String fourthElementValue = splitValue[i + 4].trim();
                            String materialCode = splitValue[9].trim();
                            String materialGroup = splitValue[8].trim();

                            boolean validateElementName = StringUtils.isAlphanumeric(elementName);
                            boolean validateFirstElementValue = NumberUtils.isNumber(firstElementValue);
                            boolean validateSecondElementValue = NumberUtils.isNumber(secondElementValue);
                            boolean validateThirdElementValue = NumberUtils.isNumber(thirdElementValue);
                            boolean validateFourthElementValue = NumberUtils.isNumber(fourthElementValue);
                            boolean validateMaterialCode = materialCode.isEmpty();
                            boolean validateMaterialGroup = materialGroup.isEmpty();

                            if (validateElementName && validateFirstElementValue && validateSecondElementValue && validateThirdElementValue && validateFourthElementValue) {

                                if (Double.parseDouble(firstElementValue) > Double.parseDouble(secondElementValue)) {

                                    String message = "Error found in line number " + lineCount + "." +
                                            "Element Name " + elementName + " has first value greater than second value. " +
                                            "First value = " + firstElementValue + ", Second element value = " + secondElementValue;

                                    MAIL_MESSAGE = MAIL_MESSAGE + message + "\n";
                                    validatorCount++;

                                }

                                if (Double.parseDouble(thirdElementValue) > Double.parseDouble(fourthElementValue)) {

                                    String message = "Error found in line number " + lineCount + "." +
                                            "Element Name " + elementName + " has third value greater than fourth value. " +
                                            "First value = " + thirdElementValue + ", Second element value = " + fourthElementValue;

                                    MAIL_MESSAGE = MAIL_MESSAGE + message + "\n";
                                    validatorCount++;

                                }




                            if (validateMaterialCode && validateMaterialGroup) {

                                String message = "Error found in line number " + lineCount + "." + " Invalid material code or material group." +
                                        "MATERIAL CODE = " + materialCode + ", MATERIAL GROUP = " + materialGroup;

                                MAIL_MESSAGE = MAIL_MESSAGE + message + "\n";
                                validatorCount++;
                            }


//                            if ((!validateElementName ||
//                                    !validateFirstElementValue ||
//                                    !validateSecondElementValue ||
//                                    !validateThirdElementValue || !validateFourthElementValue)
//                                    ) {
//
//                                validatorCount++;
//                                String message = "Error found in line number " + lineCount + "." +
//                                        " Element name " + elementName + " with value  "
//                                        + firstElementValue + "  " + secondElementValue + "  " + thirdElementValue + "  " + fourthElementValue + " are not in valid format.";
//
//                                MAIL_MESSAGE = MAIL_MESSAGE + message + "\n";
//
//                                // lineCount gives the line number in which error occured
//
//                            }


                        }

                            else {

                                String message = "Error found in line number " + lineCount + "." +
                                        " Element name " + elementName + " with value  "
                                        + firstElementValue + "  " + secondElementValue + "  " + thirdElementValue + "  " + fourthElementValue + " are not in valid format.\n";
                                String subject = "INVALID INPUT CSV FORMAT";
                                MAIL_MESSAGE = MAIL_MESSAGE + "Error in line number " + lineCount + "." + " " + message + "\n";
                                validatorCount++;
                                //alert for mismatch in number of elements
                            }


                    }
                    }
                    else {

                        String message = "Number of elements value not equal to number of elements present in threshold file.\n";
                        String subject = "INVALID INPUT CSV FORMAT";
                        MAIL_MESSAGE = MAIL_MESSAGE + "Error in line number " + lineCount + "." + " " + message + "\n";
                        validatorCount++;
                        //alert for mismatch in number of elements
                    }
                } else {
                    String message = "Invalid number of elements format in threshold file.\n";
                    String subject = "INVALID THRESHOLD INPUT FORMAT";
                    MAIL_MESSAGE = MAIL_MESSAGE + "Error in line number " + lineCount + "." + " " + message + "\n";
                    validatorCount++;
                    //alert for invalid number of elements format
                }
                }
                else{
                    String message = "Threshold line have length less than 18.";
                    String subject = "INVALID THRESHOLD FILE FORMAT";
                    MAIL_MESSAGE = MAIL_MESSAGE + "Error in line number " + lineCount + "." + " " + message + "\n";
                    validatorCount ++ ;
                }
                temp = reader.readLine();




        }
            //while end

        } catch (Exception e) {
            e.printStackTrace();
        }
        String subject = "INVALID THRESHOLD FILE FORMAT";


        if (validatorCount != 0) {
            new LogWriter().logger(config.getString("threshold_invalid_format"), true, "Logged on " + new Date() + MAIL_MESSAGE + "\n\n");

            new WatcherService().mailer("Filename : " + fileName + "\n\n" + MAIL_MESSAGE, subject, config.getString("email_recipient_list"));
            return false;
        } else return true;
    }

    public boolean isAlpha(String name) {
        return name.matches("[a-zA-Z]+");
    }

    public boolean checkPointToPercentConversionFile(String filePathInLocal, String fileName){

        BufferedReader reader = null;
        boolean isValidCSVFormat = true;
        String MAIL_MESSAGE = "";
        int lineCount = 0;

        filePathInLocal = filePathInLocal + fileName;
        int validatorCount=0;
        long fileSize = new File(filePathInLocal).length();
        try {


            if (true) {

                reader = new BufferedReader(new FileReader(filePathInLocal));
                String temp = "";
                temp = reader.readLine();
                while (temp != null) {
                    lineCount++;
                    String[] splitValue = temp.split(",");
                    if (splitValue.length >= 3){
                        String elementName = splitValue[1];
                        String elementConversionValue = splitValue[3];
                        System.out.println(elementName + "   " + elementConversionValue);
                        if (!StringUtils.isAlphanumeric(elementName) || !NumberUtils.isNumber(elementConversionValue)){
                            validatorCount++;
                            MAIL_MESSAGE = MAIL_MESSAGE + "Invalid element or value format in line number " + lineCount + "\n";
                        }
                    }
                    else {
                        //alert for length less than 3
                        MAIL_MESSAGE = MAIL_MESSAGE + "Invalid CSV length in line number " + lineCount + "\n";
                     //   new WatcherService().mailer("Invalid CSV length.","INVALID POINT TO PERCENT CONVERSION FILE",config.getString("email_recipient_list"));
                        validatorCount++;
                        isValidCSVFormat =false;
                    }
                    temp = reader.readLine();
                }
            }
            else {
                MAIL_MESSAGE = MAIL_MESSAGE + "Input file is empty\n";
                validatorCount++;
                new WatcherService().mailer("Input file is empty.","INVALID POINT TO PERCENT CONVERSION FILE",config.getString("email_recipient_list"));
                isValidCSVFormat =false;
                //alert for empty file
            }
        }

        catch (Exception e) {
            e.printStackTrace();
        }

        if(validatorCount!=0){
            System.out.println("Incorrect file uploaded");
            new WatcherService().mailer(MAIL_MESSAGE,"INVALID POINT-TO-PERCENT-CONVERSION FILE UPLOADED",config.getString("email_recipient_list"));
        return false;
        }
        else{
            System.out.println("Correct file uploaded");
            new WatcherService().mailer(MAIL_MESSAGE,"NEW POINT-TO-PERCENT-CONVERSION FILE UPLOADED",config.getString("email_recipient_list"));
            return true;
        }
    }


    public boolean validateXRFIndexes(String temp){

        String[] splitValue = temp.split(",");
       // if()

        return false;
    }

    public boolean validateOESIndexes(String temp){


        return false;
    }
}
