package com.hinditron.pipeline.spark.utility;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

/**
 * Created by prannoysircar on 7/26/16.
 */
public class PointToPercentConversionUpload {
    Config config = ConfigFactory.load("dev.conf");

    public void newFileLookup()throws IOException,
            InterruptedException{

        String fileSource = config.getString("original_point_to_percent_conversion_file_path");


        Path faxFolder = Paths.get(fileSource);
        WatchService watchService = FileSystems.getDefault().newWatchService();
        faxFolder.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY,StandardWatchEventKinds.ENTRY_CREATE);

        boolean valid = true;
        do {
            WatchKey watchKey = watchService.take();

            for (WatchEvent event : watchKey.pollEvents()) {
                WatchEvent.Kind kind = event.kind();
                if (StandardWatchEventKinds.ENTRY_MODIFY.equals(event.kind()) || StandardWatchEventKinds.ENTRY_CREATE.equals(event.kind())) {
                    String fileName = event.context().toString();
                    try {
                        String filePathInLocal = fileSource + "/" + fileName;
                        if(!fileName.contains(".swp") && !fileName.contains("4913") && fileName.contains("point")){
                            boolean checkFileFormatStatus = new FormatValidator().checkPointToPercentConversionFile(fileSource + "/" , fileName);
                            if(checkFileFormatStatus) {
                                String lastModifiedTimeStamp = new OutlierFileDetection().getLastModifiedTimeStamp(filePathInLocal, fileName);
                                File newFile = new File(filePathInLocal);
                                File copiedFile = new File(config.getString("copied_point_to_percent_conversion_file_path") + "/" + "point_to_percent_conversion.csv");
                                FileUtils.copyFile(newFile, copiedFile);
                                System.out.println("File copied to copied location");
                                File backupFile = new File(config.getString("backup_point_to_percent_conversion_file_path") + "/" + "point_to_percent_conversion.csv" + "_" + lastModifiedTimeStamp);
                                FileUtils.copyFile(newFile, backupFile);
                                System.out.println("File copied to backup location");
                                new WatcherService().mailer("New conversion file is uploaded","NEW CONVERSION FILE UPLOADED",config.getString("email_recipient_list"));

                            }

                            else {
                                //Send alert mail for format issues
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            valid = watchKey.reset();

        } while (valid);

    }
}
