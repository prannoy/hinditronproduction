package com.hinditron.pipeline.spark.utility;

/**
 * Created by prannoysircar on 6/1/16.
 */


import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.io.IOUtils;

public class SftpToHdfsTransfer {

    Config config = ConfigFactory.load("dev.conf");

    public  void monitorSFTPSourceDirectory() throws IOException,
            InterruptedException {


        getOlderFiles(config.getString("sftp_gate_way_source_path"));
        String SFTP_INPUT_SOURCE = config.getString("sftp_gate_way_source_path");
        String SFTP_INVALID_INPUT_SOURCE = config.getString("invalid_sftp_source_path");
        Path SFTP_INPUT_SOURCE_PATH = Paths.get(SFTP_INPUT_SOURCE);
        WatchService watchService = FileSystems.getDefault().newWatchService();
        SFTP_INPUT_SOURCE_PATH.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);

        boolean valid = true;
        do {
            WatchKey watchKey = watchService.take();

            for (WatchEvent event : watchKey.pollEvents()) {
                WatchEvent.Kind kind = event.kind();
                if (StandardWatchEventKinds.ENTRY_CREATE.equals(event.kind())) {
                    String fileName = event.context().toString();
                    System.out.println("Entry of file " + fileName + " found.");
                    try {

                            boolean isCSVCorrect = new FormatValidator().checkCSVformat(SFTP_INPUT_SOURCE,fileName);
                            if (isCSVCorrect){
                        // Copy file from local to hdfs
                            System.out.println("Copying ot HDFS.");
                            copyFileFromLocalToHDFS(SFTP_INPUT_SOURCE, fileName);
                        }

                        else{
                                moveFileInLocal(SFTP_INPUT_SOURCE,SFTP_INVALID_INPUT_SOURCE, fileName);
                              //  new WatcherService().mailer(fileName + " is not a valid CSV. File is skipped.", "INVALID CSV ENTRY FOUND",config.getString("email_recipient_list"));
                            }

                    } catch (Exception e) {
                        e.printStackTrace();
                        // TODO: handle exception
                    }
                }
            }
            valid = watchKey.reset();

        } while (valid);

    }

    public void copyFileFromLocalToHDFS(String LOCAL_SOURCE, String FILENAME) {

        String HDFS_HOST = config.getString("hdfs_host");
        String HDFS_DEST = config.getString("hdfs_csv_backup");

        try {

            // Input stream for the file in local file system to be written to
            // HDFS
            InputStream in = new BufferedInputStream(new FileInputStream(
                    LOCAL_SOURCE + "/" + FILENAME));

            // Get configuration of Hadoop system
            Configuration conf = new Configuration();
            conf.set("fs.defaultFS", HDFS_HOST);
            conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");

            // Destination file in HDFS
            org.apache.hadoop.fs.FileSystem fs = org.apache.hadoop.fs.FileSystem.get(conf);
            OutputStream out = fs.create(new org.apache.hadoop.fs.Path(HDFS_HOST + HDFS_DEST + FILENAME));

            // Copy file from local to HDFS
            IOUtils.copyBytes(in, out, 4096, true);

            System.out.println(FILENAME + " copied to HDFS directory " + HDFS_DEST);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void deleteFileFromHDFS(String HDFS_PATH, String FILENAME) {

        String HDFS_HOST = config.getString("hdfs_host");
        String HDFS_DEST = config.getString("hdfs_csv_backup");

        try {

            // Input stream for the file in local file system to be written to
            // HDFS

            // Get configuration of Hadoop system
            Configuration conf = new Configuration();
            conf.set("fs.defaultFS", HDFS_HOST);
            conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");

            // Destination file in HDFS
            org.apache.hadoop.fs.FileSystem fs = org.apache.hadoop.fs.FileSystem.get(conf);

            fs.delete(new org.apache.hadoop.fs.Path(HDFS_HOST + HDFS_PATH + FILENAME),false);

            System.out.println(FILENAME + " copied to HDFS directory " + HDFS_DEST);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    public void deleteFileFromHDFS (String HDFS_HOST, String HDFS_PATH, String fileName){
//
//        Configuration conf = new Configuration();
//        conf.set("fs.defaultFS", HDFS_HOST);
//        conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
//        System.out.println("Connecting to -- " + conf.get("fs.defaultFS"));
//        org.apache.hadoop.fs.Path pathToDelete = new org.apache.hadoop.fs.Path(HDFS_HOST + HDFS_PATH + "/" +fileName);
//        System.out.println("Path to delete " + pathToDelete.toUri());
//
//        try{
//        FileSystem fs = FileSystem.get(conf);
//          // if(fs.exists(pathToDelete)){
//               System.out.println("Inside " + fileName);
//            System.out.println("Inside " + fs.getUri());
//            fs.delete(pathToDelete, false);
//       }
//
//       catch (Exception e) {
//
//           e.printStackTrace();
//       }
//    }

    public void deleteFileFromLocal(String PATH, String fileName){
        java.io.File fileToDelete = new java.io.File(PATH + fileName);
        fileToDelete.delete();
    }

    public void moveFileInLocal(String SOURCE_PATH, String DEST_PATH, String FILENAME){
        try{

            System.out.println("Inside moveFile");

        File fileSourcePath = new File(SOURCE_PATH +  FILENAME);
        File fileDestPath = new File(DEST_PATH  + FILENAME);

            fileSourcePath.renameTo(fileDestPath);
        if(fileDestPath.exists()){
            System.out.println("CSV " + FILENAME + "moved to " + DEST_PATH + " folder.");
        }}
        catch (Exception e){
            e.printStackTrace();
        }


    }

    public void getOlderFiles(String INPUT_PATH){

        File folder = new File(INPUT_PATH);
        File[] listOfFiles = folder.listFiles();

        List<String> globalListOfFiles = new ArrayList<String>();

        try{
            System.out.println("INSIDE MAILER");
            String command = "/home/administrator/hinditron/get_last_processed_filename";
            ProcessBuilder processBuilder = new ProcessBuilder("bash", "-c", command);
            processBuilder.start();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        while (listOfFiles.length > 0) {


            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile() && !globalListOfFiles.contains(listOfFiles[i].getName())) {
                    globalListOfFiles.add(listOfFiles[i].getName());
                    if (new FormatValidator().checkCSVformat(INPUT_PATH, listOfFiles[i].getName())) {
                        copyFileFromLocalToHDFS(INPUT_PATH, listOfFiles[i].getName());
                    } else {
                        System.out.println("File not valid");

                    }
                    //moveFileInLocal(INPUT_PATH, DEST_PATH, listOfFiles[i].getName());
                }
            }
            listOfFiles = folder.listFiles();
        }
        System.out.println("OLDER FILES MOVED");
    }
}


