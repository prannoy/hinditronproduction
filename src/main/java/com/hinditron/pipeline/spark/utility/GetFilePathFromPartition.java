package com.hinditron.pipeline.spark.utility;

import org.apache.spark.Partition;
import org.apache.spark.rdd.UnionPartition;
//import org.apache.spark.streaming.api.java.JavaDStream;
//import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.rdd.NewHadoopPartition;

/**
 * Created by prannoysircar on 4/1/16.
 */
public class GetFilePathFromPartition {

    public String getFilePath(Partition partition) {
        UnionPartition upp = (UnionPartition) partition;
        NewHadoopPartition npp = (NewHadoopPartition) upp.parentPartition();
        String filePath = npp.serializableHadoopSplit().value().toString();

        System.out.println("*****************" + filePath);

        //file:/Users/prannoysircar/hinditron/data/csv/OES2_OES_2016-03-07_12h56m20s.csv:0+483
        return filePath;
    }
}
