package com.common

import org.apache.spark.SparkConf


/**
  * Created by prannoysircar on 6/11/16.
  */
object PrintConf {

  def printSparkConf (sparkConf: SparkConf , KEY_description : String, VALUE_description :String): Unit ={
    val getAllConf = sparkConf.getAll
    println("##### Picked spark configuration with key-value pairs #####")
    getAllConf.foreach(row => println( KEY_description + "("+ row._1 + ") " + VALUE_description + "("+ row._2 +")"))
  }

}
