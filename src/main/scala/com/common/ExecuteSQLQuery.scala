package com.common

import java.sql._
import com.hinditron.pipeline.spark.utility.{WatcherService, LogWriter}
import com.typesafe.config.ConfigFactory

/**
 * Created by prannoysircar on 3/27/16.
 */
class ExecuteSQLQuery(HOST :String, PORT :Int, DBNAME :String, USER :String, PASSWORD :String) {

  lazy val config = ConfigFactory.load("dev.conf")

  lazy val MYSQL_DUPLICATE_PK_CODE = 1062;
  lazy val JDBC_DRIVER = "com.mysql.jdbc.Driver"
  lazy val DB_URL = "jdbc:mysql://" + HOST + ":" + PORT + "/" + DBNAME
  Class.forName("com.mysql.jdbc.Driver")

  lazy val conn: Connection = DriverManager.getConnection(DB_URL, USER, PASSWORD)
  lazy val stmt: Statement = conn.createStatement()

   def executeQuery(QUERY :String, logPath: String,fileName :String): Unit = {
    try {
      println(QUERY)
      stmt.executeUpdate(QUERY)
    } catch {
      case se: SQLException => {
        getSQLException(se,logPath,fileName)
      }
    }
    closeDBConnection
  }

    def executeBatchQuery(PSEUDO_QUERY_ARRAY : scala.Array[String], logPath: String, fileName :String): Unit = {
      try {
        PSEUDO_QUERY_ARRAY.foreach(query => stmt.addBatch(query))
        stmt.executeBatch()
      } catch {
        case se: SQLException => {
          getSQLException(se,logPath,fileName)
        }
      }
      closeDBConnection
    }
    def getResultSetMap(QUERY :String, logPath: String, fileName : String): Map[String,String] ={

    var resultSet : ResultSet = null
    var map :Map[String,String] = Map()

    try {
      println(QUERY)
      resultSet = stmt.executeQuery(QUERY)
            while(resultSet.next()){
              map += ( resultSet.getString("PSEUDO_ELEMENT_NAME") -> resultSet.getString("FORMULA"))
            }
    } catch {
      case se: SQLException => {
       getSQLException(se,logPath,fileName)
      }
    }
    closeDBConnection

    map

  }

  def closeDBConnection: Unit ={
    // close DB connection
    try {
      if (stmt != null)
        conn.close()
    } catch {
      case se: SQLException => ;
    }
    try {
      if (conn != null)
        conn.close()
    } catch {
      case se: SQLException => se.printStackTrace()
    }
  }

  def getSQLException(se : SQLException, logPath :String,fileName :String): Unit ={
    println("ERROR KEY CODE " + se.getErrorCode())
    if(se.getErrorCode() == MYSQL_DUPLICATE_PK_CODE ){

      println("Cannot write to table. Duplicate key found.")
    }
    if (logPath.equals("sample_position_sql_log_path")){
      new WatcherService().mailer("DB_NAME : Analytics\nTABLE_NAME : sample_positions\n\nXML_LINE : " + fileName + " " + se + "\n", "ERROR FOUND WHILE WRITING TO DB",config.getString("email_recipient_list"))
      new LogWriter().logger(config.getString(logPath), true, "DB_NAME : Analytics\nTABLE_NAME : sample_positions\n\nXML_LINE : " + fileName + " " + se + "\n")
    }
    else if (logPath.equals("sample_results_sql_log_path")){
      new WatcherService().mailer( "DB_NAME : Analytics\nTABLE_NAME : sample_results\n\nCSV_FILE_NAME : " + fileName + " " + se + "\n", "ERROR FOUND WHILE WRITING TO DB",config.getString("email_recipient_list"))
      new LogWriter().logger(config.getString(logPath), true, "DB_NAME : Analytics\nTABLE_NAME : sample_results\n\nCSV_FILE_NAME : " + fileName + " " + se + "\n")
    }
    else if (logPath.equals("computations_sql_log_path")){
      new WatcherService().mailer( "DB_NAME : Analytics\nTABLE_NAME : computations\n\nCSV_FILE_NAME : " + fileName + " " + se + "\n", "ERROR FOUND WHILE WRITING TO DB",config.getString("email_recipient_list"))
      new LogWriter().logger(config.getString(logPath), true, "DB_NAME : Analytics\nTABLE_NAME : computations\n\nCSV_FILE_NAME : " + fileName + " " + se + "\n")
    }
   // new WatcherService().mailer( "FILE_NAME : " + fileName + " " + se + "\n", "ERROR FOUND WHILE WRITING TO DB",config.getString("email_recipient_list"))
  }



}
