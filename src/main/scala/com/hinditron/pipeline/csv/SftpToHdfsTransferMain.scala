package com.hinditron.pipeline.csv

import com.hinditron.pipeline.spark.utility.SftpToHdfsTransfer


/**
  * Created by prannoysircar on 7/21/16.
  */
object SftpToHdfsTransferMain {
  def main(args: Array[String]) {
    val obj = new SftpToHdfsTransfer()
    obj.monitorSFTPSourceDirectory()
  }
}
