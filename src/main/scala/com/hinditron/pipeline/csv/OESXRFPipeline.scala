package com.hinditron.pipeline.csv

import org.apache.hadoop.io.{Text, LongWritable}
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import com.hinditron.pipeline.spark.utility._
import java.io.{File, FileWriter, PrintWriter}
import java.util.Date
import com.common.{PrintConf, ExecuteSQLQuery}
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.elasticsearch.spark.rdd.EsSpark
import com.typesafe.config.ConfigFactory
import org.apache.hadoop.fs.Path

/**
  * Created by prannoysircar on 3/4/16.
  */


object OESXRFPipeline {

  def main(args: scala.Array[String]): Unit = {

    println("Starting OES Pipeline")

    val config = ConfigFactory.load("dev.conf")
    var GLOBAL_FILE_NAME = ""


    val conf = new SparkConf()
      .setAppName(config.getString("spark_csv_appname"))
      .set("es.nodes", config.getString("es_nodes"))
      .setMaster(config.getString("spark_master_url"))
      .setSparkHome(config.getString("spark_home_dir"))
      .set("spark.executor.memory", config.getString("spark_csv_executor_memory"))
      .set("spark.cores.max", config.getString("spark_csv_cores_max"))
      .set("spark.ui.port", config.getString("spark_csv_ui_port"))
      .set("spark.executor.uri", config.getString("spark_executor_uri"))
      .set("spark.serializer", config.getString("spark_serializer"))
      .setJars(Seq(config.getString("spark_addjar_1"),config.getString("spark_addjar_2")))


    PrintConf.printSparkConf(conf, "KEY", "VALUE")

    val ssc = new StreamingContext(conf, Seconds(config.getInt("spark_csv_streaming_time")))
    val dStream = ssc.fileStream[LongWritable, Text, TextInputFormat](config.getString("csv_data_source"),((p:Path) => true), false)

    //    val dStream = ssc.textFileStream(config.getString("csv_data_source"))
    //    ssc.checkpoint(config.getString("check_point_dir"))


    val dStreamReturn = dStream.foreachRDD(foreachFunc = rdd => {

      val lengthOfPartition = rdd.partitions.length
      val listOfPartitions = rdd.partitions
      println("Length of partition " + lengthOfPartition)
      var listOfFileName: List[String] = List()
      for (partition <- listOfPartitions) {
        val obj: GetFilePathFromPartition = new GetFilePathFromPartition
        val filePath = obj.getFilePath(partition)
        val fileName = filePath.substring(filePath.lastIndexOf("/") + 1).split(":")(0)
        listOfFileName = listOfFileName :+ fileName
      }


      val sc = ssc.sparkContext
      sc.addJar(config.getString("project_jar_path"))

      if (listOfFileName.length > 0)
        new LogWriter().logger(config.getString("csv_pipeline_latency"), true, "BATCH_START_TIME : " + new Date() + " BATCH_SIZE : " + listOfFileName.length + "  ")



      for (fileName <- listOfFileName) {
        GLOBAL_FILE_NAME = fileName
        val batch = sc.textFile(config.getString("csv_data_source") + "/" + fileName).cache()
        //        sc.addFile(config.getString("csv_data_source") + "/" + fileName)

        val pairResult = batch.flatMap(line => {
          val splitValue = line.split(",")

          var createdTime = ""
          var analysisType = ""
          var analysisTask = ""
          var analysisMethod = ""
          var sID = ""
          var numberOfElements = 0
          var date = ""
          var hourMinute = ""
          var measuredTime = ""
          var materialCode = ""
          var materialGroup = ""

          val source = fileName.substring(0, 4)
          try {

            if (source.contains("OES")) {
              val createdTimeChangedDateFormat = splitValue(OESColumns.SID_4_18.id).split("-")
              createdTime = Seq(Seq(createdTimeChangedDateFormat(2), createdTimeChangedDateFormat(1), createdTimeChangedDateFormat(0)).mkString("-"), splitValue(OESColumns.SID_5_19.id)).mkString(" ")
              analysisType = splitValue(OESColumns.NORMAL_ANALYSIS_0.id)
              analysisTask = splitValue(OESColumns.TASK_7.id)
              analysisMethod = splitValue(OESColumns.ANALYTICAL_METHOD_8.id)
              sID = splitValue(OESColumns.SID_1_15.id) + "-" + splitValue(OESColumns.SID_2_16.id) + "-" + splitValue(OESColumns.SID_3_17.id) + "_" + splitValue(OESColumns.SID_4_18.id) + " " + splitValue(OESColumns.SID_5_19.id)
              numberOfElements = splitValue(OESColumns.TOTAL_NUMBER_OF_ELEMENTS_24.id).toInt
              date = "20" + splitValue(OESColumns.MEASURED_DATE_AND_TIME_YEAR_5.id) + "-" + splitValue(OESColumns.MEASURED_DATE_AND_TIME_MONTH_4.id) + "-" + splitValue(OESColumns.MEASURED_DATE_AND_TIME_DAY_3.id)
              hourMinute = splitValue(OESColumns.MEASURED_DATE_AND_TIME_HOUR_1.id) + ":" + splitValue(OESColumns.MEASURED_DATE_AND_TIME_MINUTE_2.id)
              measuredTime = Seq(date, hourMinute).mkString(" ")

              if (!splitValue(OESColumns.MATERIAL_GROUP_22.id).isEmpty)
                materialGroup = splitValue(OESColumns.MATERIAL_GROUP_22.id)
              else
                materialGroup = " "
              if (!splitValue(OESColumns.MATERIAL_CODE_23.id).isEmpty)
                materialCode = splitValue(OESColumns.MATERIAL_CODE_23.id)
              else
                materialCode = " "

            }

            else if (source.contains("XRF")) {
              val createdTimeChangedDateFormat = splitValue(XRFColumns.DATE_17.id).split("-")
              createdTime = Seq(Seq(createdTimeChangedDateFormat(2), createdTimeChangedDateFormat(1), createdTimeChangedDateFormat(0)).mkString("-"), splitValue(XRFColumns.SID_FIELD_1_19.id)).mkString(" ")
              analysisType = splitValue(XRFColumns.NORMAL_ANALYSIS_0.id)
              analysisTask = splitValue(XRFColumns.ANALYSIS_TASK_6.id)
              analysisMethod = splitValue(XRFColumns.ANALYSIS_METHOD_7.id)
              sID = splitValue(XRFColumns.SAMPLE_ID_CHANNEL_1_14.id) + "-" + splitValue(XRFColumns.SAMPLE_ID_CHANNEL_2_15.id) + "-" + splitValue(XRFColumns.SAMPLE_POINT_16.id) + "_" + splitValue(XRFColumns.DATE_17.id) + " " + splitValue(XRFColumns.TIME_18.id)
              numberOfElements = splitValue(XRFColumns.TOTAL_NUMBER_OF_ELEMENTS_24.id).toInt
              date = "20" + splitValue(XRFColumns.MEASURED_DATE_AND_TIME_YEAR_5.id) + "-" + splitValue(XRFColumns.MEASURED_DATE_AND_TIME_MONTH_4.id) + "-" + splitValue(XRFColumns.MEASURED_DATE_AND_TIME_DAY_3.id)
              hourMinute = splitValue(XRFColumns.MEASURED_DATE_AND_TIME_HOUR_1.id) + ":" + splitValue(XRFColumns.MEASURED_DATE_AND_TIME_MINUTE_2.id)
              measuredTime = Seq(date, hourMinute).mkString(" ")
              if (!splitValue(XRFColumns.MATERIAL_GROUP_22.id).isEmpty)
                materialGroup = splitValue(XRFColumns.MATERIAL_GROUP_22.id)
              else
                materialGroup = " "
              if (!splitValue(XRFColumns.MATERIAL_CODE_23.id).isEmpty)
                materialCode = splitValue(XRFColumns.MATERIAL_CODE_23.id)
              else
                materialCode = " "


            }

          }
          catch {
            case e2: Exception => {
              new LogWriter().logger(config.getString("oes_log_error"), true, "ERROR_IN_FILE : " + fileName + "\n" + "EXCEPTION_TYPE :  " + e2)
              e2.printStackTrace(new PrintWriter(new FileWriter(config.getString("oes_log_exception")), true))
              e2.printStackTrace
            }
          }
          val initPos = 25
          val finalPos = splitValue.length - 1
          val step = 3

          val constantFields = Seq(createdTime, measuredTime, analysisType, analysisTask, analysisMethod, sID, numberOfElements).mkString(",")

          /**
            * Array, where each index correspond to a element.
            **/
          val elementArray = Array.ofDim[Double](100)
          val queryArray = new Array[String](100)

          var count = 0

          val tempList = Range.apply(initPos, finalPos, step).map { index =>

            /**
              * Set element name
              **/
            val elementName = splitValue(index)


            /**
              * Set element value
              *
              * Index of element value is always plus 2
              * i.e. if index of element name is "a" index of element value will be a+2
              *
              **/
            val elementValue = splitValue(index + 2).toDouble

            if (ElementToIndexMapping.exists(_._1 == elementName.toUpperCase())) {

              val elementIndex = ElementToIndexMapping.get(elementName.toUpperCase()).get
              elementArray(elementIndex) = elementValue
            }

            /**
              * Set value of flag based on the flag sign
              *
              * Index of flag is one value ahead of index of element name i.e. a+1
              **/

            val flagValue =
              if (FlagSymbolToValueMapping.exists(_._1 == splitValue(index + 1))) {
                val flagSign = splitValue(index + 1)
                FlagSymbolToValueMapping.get(flagSign).get
              }
              else {
                ""
              }

            val query = "insert into sample_results " +
              "(created_time," +
              "measured_time," +
              "analysis_type," +
              "analysis_task," +
              "analysis_method," +
              "sid," +
              "number_of_elements," +
              "element_name," +
              "element_value," +
              "element_flag," +
              "source," +
              "material_group," +
              "material_code)" +
              "values ('" + createdTime + "','" + measuredTime + "','" + analysisType + "','" + analysisTask + "','" + analysisMethod + "','" + sID + "','" + numberOfElements + "','" + elementName + "','" + elementValue + "','" + flagValue + "','" + source + "','" + materialGroup + "','" + materialCode + "')"



            queryArray(count) = query
            count = count + 1

            Seq(constantFields, elementName, elementValue, flagValue, source, materialGroup, materialCode).mkString(",")
          }

          new ExecuteSQLQuery(
            config.getString("sql_hostname"),
            config.getInt("sql_port"),
            config.getString("sql_database_name"),
            config.getString("sql_username"),
            config.getString("sql_password"))
            .executeBatchQuery(queryArray,"sample_results_sql_log_path",fileName)

          val obj: PseudoElementComputation = new PseudoElementComputation(constantFields, source, elementArray)
          obj.compute(fileName)

          tempList
        })

        pairResult.cache()

        val esMappedRDD = pairResult.map(line => {
          val splitValue = line.split(",")

          val createdTime = splitValue(0)
          val measuredTime = splitValue(1)
          val analysisType = splitValue(2)
          val analysisTask = splitValue(3)
          val analysisMethod = splitValue(4)
          val sID = splitValue(5)
          val numberOfElements = splitValue(6)
          val elementName = splitValue(7)
          val elementValue = splitValue(8)
          val flagValue = splitValue(9)
          val source = splitValue(10)
          val materialGroup = splitValue(11)
          val materialCode = splitValue(12)

          Map(
            "CREATED_TIME" -> createdTime,
            "MEASURED_TIME" -> measuredTime,
            "ANALYSIS_TYPE" -> analysisType,
            "ANALYSIS_TASK" -> analysisTask,
            "ANALYSIS_METHOD" -> analysisMethod,
            "SID" -> sID,
            "NUMBER_OF_ELEMENTS" -> numberOfElements,
            "ELEMENT_NAME" -> elementName,
            "ELEMENT_VALUE" -> elementValue,
            "ELEMENT_FLAG" -> flagValue,
            "SOURCE" -> source,
            "MATERIAL_GROUP" -> materialGroup,
            "MATERIAL_CODE" -> materialCode
          )
        })
        EsSpark.saveToEs(esMappedRDD, config.getString("es_index_type_name"))

      }
      //For loop end

      if(listOfFileName.length > 0){
        new LogWriter().logger(config.getString("csv_pipeline_latency"), true, "BATCH_END_TIME : " + new Date() + "\n")

        for(fileNameInList <- listOfFileName){
          new CreateCSVReport().createReportFile(fileNameInList)
          new File(config.getString("sftp_gate_way_source_path") + fileNameInList).
            renameTo(new File(config.getString("sftp_gate_way_backup_source_path" )+ fileNameInList))

        //  new SftpToHdfsTransfer().deleteFileFromHDFS(config.getString("hdfs_csv_backup"), fileNameInList)
        }
      }
    })

    dStream.print()
    ssc.start()
    ssc.awaitTermination()

    def getName(rdd: RDD[String]): Unit = {
      val listPartitions = rdd.partitions
      for (part <- listPartitions) {
        val obj: GetFilePathFromPartition = new GetFilePathFromPartition

        val filePath = obj.getFilePath(part)
        val fileName = filePath.substring(filePath.lastIndexOf("/") + 1)
        println("PATH : " + fileName)

        fileName

      }
    }
  }


}