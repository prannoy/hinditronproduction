package com.hinditron.pipeline.csv

import com.hinditron.pipeline.spark.utility.PointToPercentConversionUpload

/**
  * Created by prannoysircar on 7/26/16.
  */
object PointToPercentConversionUploadMain {

  def main(args: Array[String]) {
    val obj = new PointToPercentConversionUpload()
    obj.newFileLookup()
  }

}
