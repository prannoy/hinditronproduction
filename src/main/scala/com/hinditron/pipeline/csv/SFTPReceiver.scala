package com.hinditron.pipeline.csv

import com.common.ExecuteSQLQuery
import com.hinditron.pipeline.spark.utility.LogWriter
import com.hinditron.thermoARLcom.pipeline.spark.utility.LogWriter
import com.jcraft.jsch._
import com.typesafe.config.ConfigFactory
import org.apache.spark.Logging
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.receiver._
import com.jcraft.jsch.ChannelSftp
import java.io._
import com.sksamuel.elastic4s.ElasticClient



/**
  * Created by prannoysircar on 6/25/16.
  */
class SFTPReceiver (SFTPHOST :String,SFTPPORT :Int,SFTPUSER :String,SFTPPASS :String,SFTPWORKINGDIR :String)
  extends Receiver [String](StorageLevel.MEMORY_AND_DISK_2) with Logging{



  def onStart() {
    // Start the thread that receives data over a connection
    new Thread("Socket Receiver") {
      override def run() { receive() }
    }.start()
  }

  def onStop() {
    // There is nothing much to do as the thread calling receive()
    // is designed to stop by itself isStopped() returns false
  }



  private def receive() {
    try {
      val config2 = ConfigFactory.parseFile(new File("/home/administrator/hinditron/hinditrondev/src/main/resources/dev.conf"))
      var tempDir = "/home/administrator/hinditron/local_spark_downloads"
      val jsch = new JSch
      val session = jsch.getSession(SFTPUSER,SFTPHOST,SFTPPORT)
      session.setPassword(SFTPPASS)
      val config = new java.util.Properties()
      config.put("StrictHostKeyChecking", "no")
      session.setConfig(config)
      session.connect()
      val channel = session.openChannel("sftp")
      channel.connect()
      val channelSftp =  channel.asInstanceOf[ChannelSftp]
      channelSftp.cd(SFTPWORKINGDIR)
      val fileListFromSFTP = channelSftp.ls(SFTPWORKINGDIR)
      val esClient = ElasticClient.local

      for (i <- 0 to fileListFromSFTP.size()-1) {
        val str = fileListFromSFTP.get(i).toString.split(" ")
        val originalFilenameInSFTP: String = str(str.length - 1)
        val fileLocationLocal = tempDir + originalFilenameInSFTP
        if (originalFilenameInSFTP.contains("OES") || originalFilenameInSFTP.contains("XRF") || originalFilenameInSFTP.contains("BATCH")) {
          val buffer: Array[Byte] = new Array[Byte](1024)
          val bis: BufferedInputStream = new BufferedInputStream(channelSftp.get(originalFilenameInSFTP))
          //  val fileLocationLocal = tempDir + originalFilenameInSFTP
          val fileInLocal: File = new File(fileLocationLocal)
          val os: OutputStream = new FileOutputStream(fileInLocal)
          val bos: BufferedOutputStream = new BufferedOutputStream(os)
          var readCount: Int = 0
          System.out.println("Writing: " + originalFilenameInSFTP)

          while ((({
            readCount = bis.read(buffer);
            readCount
          })) > 0) {
            bos.write(buffer, 0, readCount)
          }
          bos.close
          bis.close
        }


        try {

          import scala.io.Source
         val finalValue = for (line <- Source.fromFile(fileLocationLocal).getLines()) {

            val splitValue = line.split(",")

            var createdTime = ""
            var analysisType = ""
            var analysisTask = ""
            var analysisMethod = ""
            var sID = ""
            var numberOfElements = 0
            var date = ""
            var hourMinute = ""
            var measuredTime = ""
            var materialCode = ""
            var materialGroup = ""

            val source = originalFilenameInSFTP.substring(0, 4)

            if (source.contains("OES")) {
              val createdTimeChangedDateFormat = splitValue(OESColumns.SID_4_18.id).split("-")
              createdTime = Seq(Seq(createdTimeChangedDateFormat(2), createdTimeChangedDateFormat(1), createdTimeChangedDateFormat(0)).mkString("-"), splitValue(19)).mkString(" ")
              analysisType = splitValue(OESColumns.NORMAL_ANALYSIS_0.id)
              analysisTask = splitValue(OESColumns.TASK_7.id)
              analysisMethod = splitValue(OESColumns.ANALYTICAL_METHOD_8.id)
              sID = splitValue(OESColumns.SID_1_15.id) + "-" + splitValue(OESColumns.SID_2_16.id) + "-" + splitValue(OESColumns.SID_3_17.id) + "_" + splitValue(OESColumns.SID_4_18.id) + " " + splitValue(OESColumns.SID_5_19.id)
              numberOfElements = splitValue(OESColumns.TOTAL_NUMBER_OF_ELEMENTS_24.id).toInt
              date = "20" + splitValue(OESColumns.MEASURED_DATE_AND_TIME_YEAR_5.id) + "-" + splitValue(OESColumns.MEASURED_DATE_AND_TIME_MONTH_4.id) + "-" + splitValue(OESColumns.MEASURED_DATE_AND_TIME_DAY_3.id)
              hourMinute = splitValue(OESColumns.MEASURED_DATE_AND_TIME_HOUR_1.id) + ":" + splitValue(OESColumns.MEASURED_DATE_AND_TIME_MINUTE_2.id)
              measuredTime = Seq(date, hourMinute).mkString(" ")
              materialGroup = splitValue(OESColumns.UNKNOWN_POSITION_22.id)
              materialCode = splitValue(OESColumns.UNKNOWN_POSITION_23.id)

            }

            else if (source.contains("XRF")) {
              val createdTimeChangedDateFormat = splitValue(XRFColumns.DATE_17.id).split("-")
              createdTime = Seq(Seq(createdTimeChangedDateFormat(2), createdTimeChangedDateFormat(1), createdTimeChangedDateFormat(0)).mkString("-"), splitValue(19)).mkString(" ")
              analysisType = splitValue(XRFColumns.NORMAL_ANALYSIS_0.id)
              analysisTask = splitValue(XRFColumns.ANALYSIS_TASK_6.id)
              analysisMethod = splitValue(XRFColumns.ANALYSIS_METHOD_7.id)
              sID = splitValue(XRFColumns.SAMPLE_ID_CHANNEL_1_14.id) + "-" + splitValue(XRFColumns.SAMPLE_ID_CHANNEL_2_15.id) + "-" + splitValue(XRFColumns.SAMPLE_POINT_16.id) + "_" + splitValue(XRFColumns.DATE_17.id) + " " + splitValue(XRFColumns.TIME_18.id)
              numberOfElements = splitValue(XRFColumns.TOTAL_NUMBER_OF_ELEMENTS_24.id).toInt
              date = "20" + splitValue(XRFColumns.MEASURED_DATE_AND_TIME_YEAR_5.id) + "-" + splitValue(XRFColumns.MEASURED_DATE_AND_TIME_MONTH_4.id) + "-" + splitValue(XRFColumns.MEASURED_DATE_AND_TIME_DAY_3.id)
              hourMinute = splitValue(XRFColumns.MEASURED_DATE_AND_TIME_HOUR_1.id) + ":" + splitValue(XRFColumns.MEASURED_DATE_AND_TIME_MINUTE_2.id)
              measuredTime = Seq(date, hourMinute).mkString(" ")
              materialGroup = splitValue(XRFColumns.SID_FIELD_4_22.id)
              materialCode = splitValue(XRFColumns.SID_FIELD_5_23.id)
            }

            else {
              new LogWriter().logger(config2.getString("log.error"), true, "ERROR_IN_FILE : Invalid Source Type : " + originalFilenameInSFTP, "")
            }

            //////

            val initPos = 25
            val finalPos = splitValue.length - 1
            val step = 3

            val constantFields = Seq(createdTime, measuredTime, analysisType, analysisTask, analysisMethod, sID, numberOfElements).mkString(",")

            /**
              * Array, where each index correspond to a element.
              **/
            val elementArray = Array.ofDim[Double](100)
            val queryArray = new Array[String](100)

            var count = 0

            val tempList = Range.apply(initPos, finalPos, step).map { index =>

              /**
                * Set element name
                **/
              val elementName = splitValue(index)


              /**
                * Set element value
                *
                * Index of element value is always plus 2
                * i.e. if index of element name is "a" index of element value will be a+2
                *
                **/
              val elementValue = splitValue(index + 2).toDouble

              if (ElementToIndexMapping.exists(_._1 == elementName.toUpperCase())) {

                val elementIndex = ElementToIndexMapping.get(elementName.toUpperCase()).get
                elementArray(elementIndex) = elementValue
              }

              /**
                * Set value of flag based on the flag sign
                *
                * Index of flag is one value ahead of index of element name i.e. a+1
                **/

              val flagValue =
                if (FlagSymbolToValueMapping.exists(_._1 == splitValue(index + 1))) {
                  val flagSign = splitValue(index + 1)
                  FlagSymbolToValueMapping.get(flagSign).get
                }
                else {
                  ""
                }

              val query = "insert into sample_results " +
                "(created_time," +
                "measured_time," +
                "analysis_type," +
                "analysis_task," +
                "analysis_method," +
                "sid," +
                "number_of_elements," +
                "element_name," +
                "element_value," +
                "element_flag," +
                "source," +
                "material_group," +
                "material_code)" +
                "values ('" + createdTime + "','" + measuredTime + "','" + analysisType + "','" + analysisTask + "','" + analysisMethod + "','" + sID + "','" + numberOfElements + "','" + elementName + "','" + elementValue + "','" + flagValue + "','" + source + "','" + materialGroup + "','" + materialCode + "')"

              queryArray(count) = query
              count = count + 1

              //Indexing to ES
              val esMap = Map(
                "CREATED_TIME" -> createdTime,
                "MEASURED_TIME" -> measuredTime,
                "ANALYSIS_TYPE" -> analysisType,
                "ANALYSIS_TASK" -> analysisTask,
                "ANALYSIS_METHOD" -> analysisMethod,
                "SID" -> sID,
                "NUMBER_OF_ELEMENTS" -> numberOfElements,
                "ELEMENT_NAME" -> elementName,
                "ELEMENT_VALUE" -> elementValue,
                "ELEMENT_FLAG" -> flagValue,
                "SOURCE" -> source,
                "MATERIAL_GROUP" -> materialGroup,
                "MATERIAL_CODE" -> materialCode
              )
             ElasticSearchQuery.ESIndex("10.0.2.15", esMap)

              Seq(constantFields, elementName, elementValue, flagValue, source, materialGroup, materialCode).mkString(",")
            }

            new ExecuteSQLQuery(
              config2.getString("sql.hostname"),
              config2.getInt("sql.port"),
              config2.getString("sql.database.name"),
              config2.getString("sql.username"),
              config2.getString("sql.password"))
              .executeBatchQuery(queryArray)

            val obj: PseudoElementComputation = new PseudoElementComputation(constantFields, source, elementArray)
            obj.compute()


            tempList



          }


          finalValue


        }


        catch {
          case e2: Exception =>
            e2.printStackTrace()
            new LogWriter().logger(config2.getString("log.error"), true, "ERROR_IN_FILE : " + originalFilenameInSFTP + "\n" + "EXCEPTION_TYPE :  " +e2, "\n")
            e2.printStackTrace(new PrintWriter(new FileWriter(config2.getString("log.exception")), true))
        }
      }

    } catch {
      case e: java.net.ConnectException =>
        // restart if could not connect to server
        restart("Error connecting to ", e)
      case t: Throwable =>
        // restart if there is any other error
        restart("Error receiving data", t)
    }
  }

}
