package com.hinditron.pipeline.csv

import com.hinditron.pipeline.spark.utility.WatcherService

/**
  * Created by prannoysircar on 7/27/16.
  */
object WatcherServiceMain {

  def main(args: Array[String]) {
    val obj = new WatcherService()
    obj.watcherServiceMainCall()
  }
}
