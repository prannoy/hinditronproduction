package com.hinditron.pipeline.csv

import java.text.SimpleDateFormat
import java.util.Date
import com.common.ExecuteSQLQuery
import com.typesafe.config.ConfigFactory

/**
  * Created by prannoysircar on 4/5/16.
  */
class PseudoElementComputation(constantFields: String, source: String, elementsValue: Array[Double]) {

  def compute(fileName: String) {

    val psuedoElementArray = Array.ofDim[Double](100)
    val psuedoElementQueryArray = new Array[String](100)


    val config = ConfigFactory.load("dev.conf")

    val C = elementsValue(1)
    val SI = elementsValue(2)
    val MN = elementsValue(3)
    val P = elementsValue(4)
    val S = elementsValue(5)
    val CR = elementsValue(6)
    val MO = elementsValue(7)
    val NI = elementsValue(8)
    val CU = elementsValue(9)
    val SN = elementsValue(10)
    val AI = elementsValue(11)
    val V = elementsValue(12)
    val B = elementsValue(13)
    val TI = elementsValue(14)
    val NB = elementsValue(15)
    val W = elementsValue(16)
    val CO = elementsValue(17)
    val SB = elementsValue(18)
    val AS = elementsValue(19)
    val TE = elementsValue(20)
    val CA = elementsValue(21)
    val N2 = elementsValue(22)
    val H2 = elementsValue(23)
    val BI = elementsValue(24)
    val O2 = elementsValue(25)
    val PB = elementsValue(26)
    val ZN = elementsValue(27)
    val ZR = elementsValue(28)
    val SE = elementsValue(29)
    val CD = elementsValue(30)
    val AL = elementsValue(31)
    val ALS = elementsValue(32)
    val ALO = elementsValue(33)
    val BS = elementsValue(34)
    val BO = elementsValue(35)
    val N = elementsValue(36)
    val LQ1 = elementsValue(37)
    val WHF = elementsValue(38)
    val FK = elementsValue(39)
    val CE = elementsValue(40)
    val MG = elementsValue(41)






    var P01: Double = 0.0
    var P02: Double = 0.0
    var P03: Double = 0.0
    var P04: Double = 0.0
    var P05: Double = 0.0
    var P06: Double = 0.0
    var P07: Double = 0.0
    var P08: Double = 0.0
    var P09: Double = 0.0
    var P10: Double = 0.0
    var P11: Double = 0.0
    var P12: Double = 0.0
    var P13: Double = 0.0
    var P14: Double = 0.0
    var P15: Double = 0.0
    var P16: Double = 0.0
    var P17: Double = 0.0
    var P18: Double = 0.0
    var P19: Double = 0.0
    var P20: Double = 0.0
    var P21: Double = 0.0
    var P22: Double = 0.0
    var P23: Double = 0.0
    var P24: Double = 0.0
    var P25: Double = 0.0
    var P26: Double = 0.0
    var P27: Double = 0.0
    var P28: Double = 0.0
    var P29: Double = 0.0
    var P30: Double = 0.0
    var P31: Double = 0.0
    var P32: Double = 0.0
    var P33: Double = 0.0
    var P34: Double = 0.0
    var P35: Double = 0.0
    var P36: Double = 0.0
    var P37: Double = 0.0
    var P38: Double = 0.0
    var P39: Double = 0.0
    var P40: Double = 0.0
    var P41: Double = 0.0
    var P42: Double = 0.0
    var P43: Double = 0.0
    var P44: Double = 0.0
    var P45: Double = 0.0
    var P46: Double = 0.0
    var P47: Double = 0.0
    var P48: Double = 0.0
    var P49: Double = 0.0
    var FARBE: Double = 0.0


    P01 = CR + MO + NI
    P02 = C + MN

    if (S != 0)
      P03 = MN / S
    P04 = CU + NI + CR + MO
    P05 = NI + MO
    P06 = CU + (10 * SN)
    P04 = CU + NI + CR + MO
    P05 = NI + MO
    P06 = CU + (10 * SN)
    P07 = P + S
    P08 = CR + MO
    P09 = CR + NI
    P10 = SI + NI + CR + CU
    P11 = V + NB
    P12 = NI + CU
    P13 = CU + (8 * SN)
    P14 = SI + MN + CR
    P15 = CR + MO + CU
    P16 = (3 * C) + (3 * CR) + (2 * MO) + NI
    P17 = CR + NI + CU
    if (N != 0)
      P18 = AL / N
    P19 = AS + SB + SN
    P20 = NB + TI + V
    P21 = CU + SN
    P22 = SI + (P * 2.5)
    if (N != 0)
      P23 = TI / N
    P24 = CU + (6 * SN)
    P25 = SI + MN + NI + CR + CU + AL
    P26 = C + (2 * MN) + CR + (2 * NI) + (6 * MO)
    P27 = CU + (5 * SN)
    P28 = (5 * C) + MN
    P29 = CR + MN
    P30 = MO + NI + CU
    P31 = C + (SI / 24) + (MN / 6) + (CR / 5) + (MO / 4) + (NI / 40) + (V / 14)
    P32 = MN + (0.35 * NI) + (1.5 * CR) + (2 * MO) + (0.17 * CU)
    P33 = P32 + (C * 3.33)
    P34 = C + (MN / 6) + ((CR + MO + V) / 5) + ((NI + CU) / 15)
    P35 = V + NB + TI + B + ZR + SN
    P36 = (750 * Math.sqrt(C)) + (150 * SI) + (250 * MN) + (150 * CR) + (300 * MO) + (120 * NI) + (200 * CU) + (600 * V)
    P37 = SI + P
    if (TE != 0)
      P38 = S / TE
    P39 = 0

    /** Calculate P39  */
    if (C <= 0.39) {
      P39 = if (MN <= 1.20) //a
        25.4 * ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) * (1.00 + (3.00 * MO)) * (1.00 + (0.365 * CU)) * (1.00 + (1.73 * V))) * (0.54 * C) * ((3.3333 * MN) + 1)
      else if (MN <= 1.95) //b
        25.4 * ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) * (1.00 + (3.00 * MO)) * (1.00 + (0.365 * CU)) * (1.00 + (1.73 * V))) * (0.54 * C) * ((5.10 * MN) - 1.12)
      else 0
    }
    else if (C <= 0.55) {
      P39 = if (MN <= 1.20) //c
        25.4 * ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) * (1.00 + (3.00 * MO)) * (1.00 + (0.365 * CU)) * (1.00 + (1.73 * V))) * (0.171 + (0.001 * C) + (0.265 * (C * C))) * ((3.3333 * MN) + 1)
      else if (MN <= 1.95) //d
        25.4 * ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) * (1.00 + (3.00 * MO)) * (1.00 + (0.365 * CU)) * (1.00 + (1.73 * V))) * (0.171 + (0.001 * C) + (0.265 * (C * C))) * ((5.10 * MN) - 1.12)
      else 0
    }
    else if (C <= 0.65) {
      P39 = if (MN <= 1.20) //e
        25.4 * ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) * (1.00 + (3.00 * MO)) * (1.00 + (0.365 * CU)) * (1.00 + (1.73 * V))) * (0.115 + (0.268 * C) - (0.038 * (C * C))) * ((3.3333 * MN) + 1)
      else if (MN <= 1.95) //f
        25.4 * ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) * (1.00 + (3.00 * MO)) * (1.00 + (0.365 * CU)) * (1.00 + (1.73 * V))) * (0.115 + (0.268 * C) - (0.038 * (C * C))) + ((5.10 * MN) - 1.12)
      else 0
    }
    else if (C <= 0.75) {
      P39 = if (MN <= 1.20) //g
        25.4 * ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) * (1.00 + (3.00 * MO)) * (1.00 + (0.365 * CU)) * (1.00 + (1.73 * V))) * (0.143 + (0.2 * C)) + ((3.3333 * MN) + 1)
      else if (MN <= 1.95) //h
        25.4 * ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) * (1.00 + (3.00 * MO)) * (1.00 + (0.365 * CU)) * (1.00 + (1.73 * V))) * (0.143 + (0.2 * C)) + ((5.10 * MN) - 1.12)
      else 0
    }
    else if (C <= 0.90) {
      P39 = if (MN <= 1.20) //i
        25.4 * ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) * (1.00 + (3.00 * MO)) * (1.00 + (0.365 * CU)) * (1.00 + (1.73 * V))) * (0.062 + (0.409 * C) - (0.135 * (C * C))) * ((3.3333 * MN) + 1)
      else if (MN <= 1.95) //j
        25.4 * ((1.00 + (0.7 * SI)) * (1.00 + (0.363 * NI)) * (1.00 + (2.16 * CR)) * (1.00 + (3.00 * MO)) * (1.00 + (0.365 * CU)) * (1.00 + (1.73 * V))) * (0.062 + (0.409 * C) - (0.135 * (C * C))) * ((5.10 * MN) - 1.12)
      else 0
    }
    else {
    }

    P40 = P39 / 25.4


    if (N <= 0.0100) {
      if (AL >= 0.020) {
        if (AL <= -0.0002 * ((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) + 0.090) {
          P41 = 3
        }
        else if (AL <= -0.0002 * ((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) + 0.110) {
          P41 = 2
        }
        else if (AL <= -0.0002 * ((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) + 0.140) {
          P41 = 1
        }
        else {
          P41 = 0
        }
      }


      else if (AL < 0.020) {
        if (((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) <= 350) {
          P41 = 3
        }
        else if (((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) <= 450) {
          P41 = 2
        }
        else if (((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) <= 600) {
          P41 = 1
        }
        else {
          P41 = 0
        }
      }

    }


    else if (N > 0.0100) {
      if (AL >= 0.020) {
        if (AL <= -0.0002 * ((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) + 0.080) {
          P41 = 3
        }
        else if (AL <= -0.0002 * ((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) + 0.100) {
          P41 = 2
        }
        else if (AL <= -0.0002 * ((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) + 0.140) {
          P41 = 1
        }
        else {
          P41 = 0
        }
      }


      else if (AL < 0.020) {
        if (((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) <= 300) {
          P41 = 3
        }
        else if (((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) <= 400) {
          P41 = 2
        }
        else if (((10 * CU * 100) + (3 * AS * 1000) + (8 * SN * 1000)) <= 600) {
          P41 = 1
        }
        else {
          P41 = 0
        }
      }
    }


    FARBE = P41

    P43 = C + SI / 28 + (MN + MO) / 3.7 + CR / 6.45

    P44 = TI - (5 * N - 0.003)

    if (AS != 0)
      P45 = 10 * SB / AS

    P46 = (MN + SI) * (P + SN) * 10000

    P47 = CR + MO + NI + CU + V

    P48 = P + SN

    P49 = 85.25 + 256 * C + 16 * SI + 31 * MN - 447.5 * S + 54 * CR + 8 * NI + 171 * MO + 184 * V + 35 * CU + 961 * N


    val dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
    val submittedDateConvert = new Date()
    val computeTime = dateFormatter.format(submittedDateConvert)

    val splitValue = constantFields.split(",")
    val createdTime = splitValue(0)
    val measuredTime = splitValue(1)
    val analysisType = splitValue(2)
    val analysisTask = splitValue(3)
    val analysisMethod = splitValue(4)
    val sID = splitValue(5)




    psuedoElementArray(1) = P01
    psuedoElementArray(2) = P02
    psuedoElementArray(3) = P03
    psuedoElementArray(4) = P04
    psuedoElementArray(5) = P05
    psuedoElementArray(6) = P06
    psuedoElementArray(7) = P07
    psuedoElementArray(8) = P08
    psuedoElementArray(9) = P09
    psuedoElementArray(10) = P10
    psuedoElementArray(11) = P11
    psuedoElementArray(12) = P12
    psuedoElementArray(13) = P13
    psuedoElementArray(14) = P14
    psuedoElementArray(15) = P15
    psuedoElementArray(16) = P16
    psuedoElementArray(17) = P17
    psuedoElementArray(18) = P18
    psuedoElementArray(19) = P19
    psuedoElementArray(20) = P20
    psuedoElementArray(21) = P21
    psuedoElementArray(22) = P22
    psuedoElementArray(23) = P23
    psuedoElementArray(24) = P24
    psuedoElementArray(25) = P25
    psuedoElementArray(26) = P26
    psuedoElementArray(27) = P27
    psuedoElementArray(28) = P28
    psuedoElementArray(29) = P29
    psuedoElementArray(30) = P30
    psuedoElementArray(31) = P31
    psuedoElementArray(32) = P32
    psuedoElementArray(33) = P33
    psuedoElementArray(34) = P34
    psuedoElementArray(35) = P35
    psuedoElementArray(36) = P36
    psuedoElementArray(37) = P37
    psuedoElementArray(38) = P38
    psuedoElementArray(39) = P39
    psuedoElementArray(40) = P40
    psuedoElementArray(41) = P41
    psuedoElementArray(42) = P42
    psuedoElementArray(43) = P43
    psuedoElementArray(44) = P44
    psuedoElementArray(45) = P45
    psuedoElementArray(46) = P46
    psuedoElementArray(47) = P47
    psuedoElementArray(48) = P48
    psuedoElementArray(49) = P49
    psuedoElementArray(50) = FARBE


    for (index <- 1 to 50) {

      val query = "insert into computations " +
        "(measured_time," +
        "sid," +
        "computation_time," +
        "pseudo_element_name," +
        "pseudo_element_value," +
        "source)" +
        "values ('" +
        measuredTime + "','" +
        sID + "','" +
        computeTime + "','" +
        PseudoElementToIndexMapping.get(index).get + "','" +
        Math.max(0, psuedoElementArray(index)) + "','" +
        source + "')"

      psuedoElementQueryArray(index - 1) = query

    }

    new ExecuteSQLQuery(
      config.getString("sql_hostname"),
      config.getInt("sql_port"),
      config.getString("sql_database_name"),
      config.getString("sql_username"),
      config.getString("sql_password"))
      .executeBatchQuery(psuedoElementQueryArray,"computations_sql_log_path",fileName)

  }

}
