package com.hinditron.pipeline.csv

import com.hinditron.pipeline.spark.utility.DeleteFilesFromHDFS

/**
  * Created by prannoysircar on 8/3/16.
  */
object DeleteFilesFromHDFSMain {
  def main(args: Array[String]) {
    val obj = new DeleteFilesFromHDFS()
    obj.watcherServiceMainCall()
  }
}
