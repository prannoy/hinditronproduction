    package com.hinditron.pipeline.csv

    import java.io.{File, FileWriter}
    import java.sql.Timestamp
    import java.util.Date

    import com.hinditron.pipeline.spark.utility.OutlierFileDetection
    import com.typesafe.config.ConfigFactory
    import org.apache.commons.lang3.math.NumberUtils

    import scala.io.Source

    /**
      * Created by prannoysircar on 7/10/16.
      */
    class CreateCSVReport() {

      val config = ConfigFactory.load("dev.conf")

      def createReportFile(fileName : String): Unit = {



//            # 0 = comma , 1 = pipe
//            get.delimiter="0"
//
//            # 0 = Normal, 1 = Percentage
//            get.conversiontype="0"
//
//            # 0 = dot, 1 = comma
//            get.decimaltype="."
        val conversion = getConversionInput()
        val delimiter = conversion.delimiter
        val conversionType = conversion.conversionType
        val decimalType = conversion.decimalType
        val point_to_percent_conversion_value_map = conversion.point_to_percent_conversion_value_map

        var finalReportLine = ""

        /**
          * Make threshold line to be written in report
          * */
        val inReportThresholdLine = generateCSVRportThresholdLine(delimiter)

        val getGenerateCSVReportCSVLineValues = generateCSVReportCSVLine(fileName, delimiter, conversionType, decimalType, point_to_percent_conversion_value_map)
        val inReportCSVLine = getGenerateCSVReportCSVLineValues._2

        /**
          * Make csv line and psuedo element line to be written in report
          **/
        val elementValueArray = getGenerateCSVReportCSVLineValues._1
        val SID = getSID(fileName)
        try {
          val obj: LocalPsuedoElementComputation = new LocalPsuedoElementComputation()
          val psuedoElementsValueArray = obj.compute(elementValueArray,conversionType,delimiter,decimalType,point_to_percent_conversion_value_map)
          val psuedoElementsValueString = obj.psuedoElementsNameToValueGenerator(psuedoElementsValueArray,delimiter,conversionType,decimalType,point_to_percent_conversion_value_map)
          finalReportLine = finalReportLine + inReportThresholdLine  + inReportCSVLine + psuedoElementsValueString
          try {
            val reportFileName = Seq(fileName.substring(0, 4), SID, new OutlierFileDetection().getDateInString()).mkString("_")
            val writer: FileWriter = new FileWriter(config.getString("handsoff_report_path") + reportFileName, false)
            writer.write(finalReportLine)
            writer.flush
            writer.close
          }
          catch {
            case e: Exception => {
              e.printStackTrace
            }
          }
        }catch {case e2: Exception => {
          e2.printStackTrace()
        }
        }


      }

      case class Result(delimiter : String, conversionType : String, decimalType : String, point_to_percent_conversion_value_map : Map[String, String])

      def getConversionInput(): Result ={
        val localConfig = ConfigFactory.parseFile(new File("/home/administrator/hinditron/hinditrondev/src/main/resources/conversion.conf"))

        //    # 0 = comma , 1 = pipe
        //    get.delimiter="0"
        //
        //    # 0 = Normal, 1 = Percentage
        //    get.conversiontype="0"
        //
        //    # 0 = dot, 1 = comma
        //    get.decimaltype="."

        val delimiterConf = localConfig.getString("delimiter")
        val conversionTypeConf = localConfig.getString("conversiontype")
        val decimalTypeConf = localConfig.getString("decimaltype")


        var delimiter = ""
        if (delimiterConf.equals("0"))
          delimiter = ","
        else
          delimiter = "|"

        var conversionType = ""
        if (conversionTypeConf.equals("0"))
          conversionType = "normal"
        else
          conversionType = "percentage"

        var decimalType = ""
        if (decimalTypeConf.equals("0"))
          decimalType = "."
        else
          decimalType = ","

        var point_to_percent_conversion_value_map :Map[String,String] = Map()

        for (line <- Source.fromFile(config.getString("point_to_percent_conversion_values_path")).getLines) {
          println(line)
          val lineSplitValue = line.split(",")
          val elementName =  lineSplitValue(1).trim
          val elementConversionValue = lineSplitValue(3).trim
          point_to_percent_conversion_value_map +=  elementName -> elementConversionValue
        }
        Result(delimiter,conversionType,decimalType,point_to_percent_conversion_value_map)
      }

      def generateCSVRportThresholdLine (delimiter : String):String = {
        val thresholdLine = Source.fromFile(config.getString("copied_threshold_file_path") + "/threshold.txt").getLines.toList.take(1).toString()
        val thresholdSplitValue = thresholdLine.split("""\|""")
        var inReportThresholdLine = ""
        for (index <- 1 to 10) {
          inReportThresholdLine = inReportThresholdLine + thresholdSplitValue(index).trim + delimiter
        }
        inReportThresholdLine
      }

      def generateCSVReportCSVLine (fileName :String, delimiter :String, conversionType : String, decimalType :String, point_to_percent_conversion_value_map :Map[String,String] ) ={

        val localElementArray = Array.ofDim[Double](100)
        val csvLine = Source.fromFile(config.getString("sftp_gate_way_source_path") + fileName).getLines.toList.take(1).toString()
        val csvSplitValue = csvLine.split(",")
        val number_of_elements = csvSplitValue(24)

        try {


          val initPos = 25
          val finalPos = csvSplitValue.length - 1
          val step = 3
          var inReportCSVLine = ""

          Range.apply(initPos, finalPos, step).map { index =>

            val elementName = csvSplitValue(index).trim
            val elementValue = csvSplitValue(index + 2).trim

            var finalElementValueInReport = elementValue
            if(conversionType.equals("percentage")){
//              if(point_to_percent_conversion_value_map.exists(_._1.toUpperCase() == elementName.toUpperCase)){
              if(point_to_percent_conversion_value_map.contains(elementName)){

                if(NumberUtils.isNumber(point_to_percent_conversion_value_map.get(elementName).get)){
                val conversionUnit = point_to_percent_conversion_value_map.get(elementName).get.trim.toDouble
                finalElementValueInReport = (elementValue.toDouble * conversionUnit).toString
                }
              }
            }
            if(decimalType.equals(",")){
              finalElementValueInReport = finalElementValueInReport.replace(".",",")
            }
            inReportCSVLine = inReportCSVLine + elementName + delimiter + finalElementValueInReport + delimiter

            if (ElementToIndexMapping.exists(_._1 == elementName.toUpperCase())) {
              val elementIndex = ElementToIndexMapping.get(elementName.toUpperCase()).get
              localElementArray(elementIndex) = elementValue.toDouble
            }
          }
          (localElementArray,number_of_elements + delimiter + inReportCSVLine)
      }
    }

      def getSID(fileName:String):String ={
        val csvLine = Source.fromFile(config.getString("sftp_gate_way_source_path") + fileName).getLines.toList.take(1).toString()
        val csvSplitValue = csvLine.split(",")
        var SID = ""
        try {
           if (fileName.contains("OES")) {
           SID = csvSplitValue(OESColumns.SID_1_15.id) + "-" + csvSplitValue(OESColumns.SID_2_16.id) + "-" + csvSplitValue(OESColumns.SID_3_17.id) + "_" + csvSplitValue(OESColumns.SID_4_18.id) + " " + csvSplitValue(OESColumns.SID_5_19.id)
          }
          else if (fileName.contains("XRF")) {
           SID = csvSplitValue(XRFColumns.SAMPLE_ID_CHANNEL_1_14.id) + "-" + csvSplitValue(XRFColumns.SAMPLE_ID_CHANNEL_2_15.id) + "-" + csvSplitValue(XRFColumns.SAMPLE_POINT_16.id) + "_" + csvSplitValue(XRFColumns.DATE_17.id) + " " + csvSplitValue(XRFColumns.TIME_18.id)
          }
        }
        SID.replaceAll(":","_").replaceAll(" ","_")
      }

    }

