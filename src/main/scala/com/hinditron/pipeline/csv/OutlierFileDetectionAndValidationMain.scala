package com.hinditron.pipeline.csv

import com.hinditron.pipeline.spark.utility.OutlierFileDetection

/**
  * Created by prannoysircar on 7/21/16.
  */
object OutlierFileDetectionAndValidationMain {
  def main(args: Array[String]) {
    val obj = new OutlierFileDetection()
    obj.newFileLookup()
  }
}
