package com.hinditron.pipeline.csv

/**
  * Created by prannoysircar on 4/5/16.
  */
object PseudoElementToIndexMapping {

  def get(pseudoIndex: Int): Option[String] = pseudoElementsMapping.get(pseudoIndex)

  def exists(p: ((Int, String)) => Boolean): Boolean = pseudoElementsMapping.exists(p)


  val pseudoElementsMapping = Map(

    1 -> ("P01"),
    2 -> "P02",
    3 -> "P03",
    4 -> "P04",
    5 -> "P05",
    6 -> "P06",
    7 -> "P07",
    8 -> "P08",
    9 -> "P09",
    10 -> "P10",
    11 -> "P11",
    12 -> "P12",
    13 -> "P13",
    14 -> "P14",
    15 -> "P15",
    16 -> "P16",
    17 -> "P17",
    18 -> "P18",
    19 -> "P19",
    20 -> "P20",
    21 -> "P21",
    22 -> "P22",
    23 -> "P23",
    24 -> "P24",
    25 -> "P25",
    26 -> "P26",
    27 -> "P27",
    28 -> "P28",
    29 -> "P29",
    30 -> "P30",
    31 -> "P31",
    32 -> "P32",
    33 -> "P33",
    34 -> "P34",
    35 -> "P35",
    36 -> "P36",
    37 -> "P37",
    38 -> "P38",
    39 -> "P39",
    40 -> "P40",
    41 -> "P41",
    42 -> "P42",
    43 -> "P43",
    44 -> "P44",
    45 -> "P45",
    46 -> "P46",
    47 -> "P47",
    48 -> "P48",
    49 -> "P49",
    50 -> "FARBE"

  )

}
