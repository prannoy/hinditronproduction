package com.hinditron.pipeline.csv

import com.common.PrintConf
import com.typesafe.config.ConfigFactory
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * Created by prannoysircar on 6/25/16.
  */
object Main {

  def main(args: Array[String]) {
    println("Starting OES Pipeline")

    val config = ConfigFactory.load("dev.conf")

    val conf = new SparkConf()
      .setAppName("CsvStream")
      .set("es.nodes", config.getString("es.nodes"))
      .setMaster(config.getString("master"))
      .setSparkHome(config.getString("spark.home.dir"))
      .set("spark.executor.memory", config.getString("spark.executor.memory"))
      .set("spark.cores.max", config.getString("spark.cores.max"))
      .set("spark.ui.port", config.getString("spark.ui.port"))
      .set("spark.executor.uri", config.getString("spark.executor.uri"))
      .set("spark.serializer", config.getString("spark.serializer"))
      .setJars(Seq(config.getString("spark.addjar.1"),config.getString("spark.addjar.2")))


    PrintConf.printSparkConf(conf, "KEY", "VALUE")

    val SFTPHOST = "ubuntu"
    val SFTPPORT = 33
    val SFTPUSER = "hinditron"
    val SFTPPASS = "Hinditron$#@!"
    val SFTPWORKINGDIR = "/home/hinditron/XRFOESFiles"

    val ssc = new StreamingContext(conf, Seconds(config.getInt("spark.streaming.time")))
    val sftpReceiver :SFTPReceiver = new SFTPReceiver(SFTPHOST ,SFTPPORT ,SFTPUSER ,SFTPPASS ,SFTPWORKINGDIR)


    ssc.receiverStream(sftpReceiver).print()

    ssc.start()
    ssc.awaitTermination()
  }

}
