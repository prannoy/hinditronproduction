package com.hinditron.pipeline.xml

/**
  * Created by prannoysircar on 4/13/16.
  */
object PostitionNumberToInstrumentNameMapping {

  def get(symbol: String): Option[String] = postitionNumberToInstrumentNameMapping.get(symbol)

  def exists(p: ((String, String)) => Boolean): Boolean = postitionNumberToInstrumentNameMapping.exists(p)

  val postitionNumberToInstrumentNameMapping = Map(
    "3101" -> "Registration,HR-HSK1,1",
    "3201" -> "Registration,HR-HSK2,1",
    "3301" -> "Registration,HR-HSK3,1",
    "110" -> "Tube Transport,HR-LSM1,2",
    "310" -> "Tube Transport,HR-LSM3,2",
    "111" -> "Carrier arrived in lab,HR-LSM1,3",
    "17103" -> "Output -  Slag sample cup,HR-LSM1",
    "210" -> "Tube Transport,HR-LSM2,2",
    "6099" -> "Robot,Sample In Robot,5",
    "6041" -> "Pos41_Cup-BeltPos,Robot,8",
    "211" -> "Carrier arrived in lab,HR-LSM2,3",
    "311" -> "Carrier arrived in lab,HR-LSM3,3",
    "22051" -> "Input Belt - Cup from Robot circle,HP-CA,9",
    "22001" -> "Input Belt - Cup belt transport,HP-CA,10",
    "22002" -> "Cup at Crusher,HP-CA,11",
    "21404" -> "On belt left,HP-CA,12",
    "17203" -> "Output -  Slag sample cup,HR-LSM2,4",
    "17303" -> "Output -  Slag sample cup,HR-LSM3,4",
    "21403" -> "On belt at lift,HP-CA,13",
    "21402" -> "In Crusher,HP-CA,14",
    "6011" -> "Cup_Buffer_Pos_1,Robot,6",
    "6012" -> "Cup_Buffer_Pos_2,Robot,6",
    "6013" -> "Cup_Buffer_Pos_3,Robot,6",
    "6014" -> "Cup_Buffer_Pos_4,Robot,6",
    "6401" -> "Cup_Mag1,Robot,7",
    "6402" -> "Cup_Mag2,Robot,7",
    "6403" -> "Cup_Mag3,Robot,7",
    "6404" -> "Cup_Mag4,Robot,7",
    "21401" -> "Output,HP-CA,15",
    "21504" -> "On Belt,HP-MA,16",
    "21503" -> "On belt at lift,HP-MA,17",
    "21505" -> "Dosing,HP-MA,18",
    "21508" -> "Magnetic separator,HP-MA,19",
    "21502" -> "Grinding,HP-MA,20",
    "21501" -> "Output,HP-MA,21",
    "21605" -> "In press,HP-PA,23",
    "21891" -> "Ring_pos1,HP-PA,26",
    "21892" -> "Ring_pos2,HP-PA,26",
    "21893" -> "Ring_pos3,HP-PA,26",
    "21894" -> "Ring_pos4,HP-PA,26",
    "21895" -> "Ring_pos5,HP-PA,26",
    "21896" -> "Ring_pos6,HP-PA,26",
    "21607" -> "Cup on belt,HP-PA,22",
    "21602" -> "Ready in press tool,HP-PA,24",
    "21603" -> "On output belt,HP-PA,25",
    "21701" -> "Ring Cleaning - Ring cleaning,HP-PA,50",
    "21704" -> "Ring Cleaning - Waste bin,HP-PA,50",
    "21617" -> "Cup return,HP-PA,50",
    "21612" -> "Cupcleaning,HP-PA,50",
    "21613" -> "Empty cup back,HP-PA,50",
    "21530" -> "Belt Start,HP-MA,50",
    "21405" -> "On belt right,HP-CA,50",
    "22003" -> "Input belt -   Cup at Robot circle,HP-CA,50",
    "6031" -> "Pos31_Slag_HR_LSM_1,Robot,50",
    "6032" -> "Pos31_Slag_HR_LSM_2,Robot,50",
    "6033" -> "Pos31_Slag_HR_LSM_3,Robot,50",
    "21809" -> "Output belt -  End of belt,HP-PA,28",
    "21814" -> "Sample On Transport Belt,HP-PA,27",
    "8801" -> "In Instrument,XRF,29",
    "8803" -> "Analysis finished,XRF,30",
    "8804" -> "Ready for output,XRF,31"
  )


}
