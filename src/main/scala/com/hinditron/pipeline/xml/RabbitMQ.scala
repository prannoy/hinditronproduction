package com.hinditron.pipeline.xml

import java.io.{FileWriter, PrintWriter}
import com.hinditron.pipeline.spark.utility.{WatcherService, LogWriter}
import com.stratio.receiver.RabbitMQUtils
import com.typesafe.config.ConfigFactory
import org.apache.spark.streaming.{StreamingContext, Seconds}
import org.apache.spark.{SparkContext, SparkConf}
import com.common.ExecuteSQLQuery


/**
  * Created by prannoysircar on 3/8/16.
  */
object RabbitMQ {
  def main(args: scala.Array[String]): Unit = {

    val config = ConfigFactory.load("dev.conf")

    val conf = new SparkConf()
      .setAppName(config.getString("spark_xml_appname"))
      .setMaster(config.getString("spark_master_url"))
      .setSparkHome(config.getString("spark_home_dir"))
      .set("spark.executor.memory", config.getString("spark_xml_executor_memory"))
      .set("spark.cores.max", config.getString("spark_xml_cores_max"))
      .set("spark.ui.port", config.getString("spark_xml_ui_port"))
      .set("spark.executor.uri", config.getString("spark_executor_uri"))
      .set("spark.serializer", config.getString("spark_serializer"))
      .setJars(Seq(config.getString("rabbitmq_receiver_jar"),config.getString("project_jar_path"),config.getString("spark_addjar_3"),config.getString("spark_addjar_4")))

    val sc = new SparkContext(conf)
    sc.addJar(config.getString("project_jar_path"))

    val ssc = new StreamingContext(sc, Seconds(5))

    val receiverStream = RabbitMQUtils.createStream(ssc, Map(
      "host" -> config.getString("queue_hostname"),
      "queueName" -> config.getString("queue_queuename"),
      "username" -> config.getString("queue_username"),
      "password" -> config.getString("queue_password"),
      "vHost" -> config.getString("vhost"))
    )

    receiverStream.foreachRDD(msg => {

      msg.foreach(line => {

        new LogWriter().logger("/home/administrator/hinditron/logs/xml-logs/xml-messages", true,line)
        new LogWriter().logToHDFS(config.getString("hdfs_host"),"/test/xml_messages/", line+"\n")

        try {
          val data = scala.xml.XML.loadString(line)
          val action = data.nonEmptyChildren(1).label
          val sid = (data \\ action \ "@sid") text
          val time = (data \\ "Interface" \ "@time") text
          val typeValue = (data \\ action \ "@type") text
          val posno = (data \\ action \ "@posno") text


          var postitionDetail = ""
          var instrumentName = ""

          val postionDetailAndInstrumentName =
            if (PostitionNumberToInstrumentNameMapping.exists(_._1.contains(posno))) {

              val machineDetail = PostitionNumberToInstrumentNameMapping.get(posno).get

              postitionDetail = machineDetail.split(",")(0)
              instrumentName = machineDetail.split(",")(1)

              machineDetail

            }
            else {
              "NOT PRESENT"
            }

          val query = "insert into sample_positions " +
            "(position_time," +
            "sid," +
            "action," +
            "type," +
            "position_number," +
            "position_name," +
            "instrument) " +
            "values ('" + time + "','" + sid + "','" + action + "','" + typeValue + "','" + posno + "','" + postitionDetail + "','" + instrumentName + "')"


          new ExecuteSQLQuery(
            config.getString("sql_hostname"),
            config.getInt("sql_port"),
            config.getString("sql_database_name"),
            config.getString("sql_username"),
            config.getString("sql_password"))
            .executeQuery(query,"sample_position_sql_log_path", line)


        }
        catch {
          case e2: Exception => {
            new LogWriter().logger(config.getString("xml_log_error"), true, "Error in line : " + line)
            new WatcherService().mailer("Error in line : " + line, "ERROR FOUND IN XML", config.getString("email_recipient_list"))
            e2.printStackTrace(new PrintWriter(new FileWriter(config.getString("xml_log_exception")), true))
          }
        }
      }

      )
    }
    )
    receiverStream.print()

    ssc.start()
    ssc.awaitTermination()

  }
}